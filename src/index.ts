//
// Copyright (c) 2021 Steven R. Baker <steven@stevenrbaker.com>
//
// This file is part of Word Counter.
//
// Word Counter is free software: you can redistribute it and/or
// modify it under the terms of the GNU Affero General Public License
// as published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// Word Counter is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
//

import joplin from 'api';

joplin.plugins.register({
    onStart: async function() {
        const panel = await joplin.views.panels.create('word_count');

        await joplin.views.panels.setHtml(panel, 'Counting words...');

        async function updateNoteWordCountView() {
            const note = await joplin.workspace.selectedNote();

            if (note) {
                const wordCountHtml = `<p>Words: <strong>${numberOfWordsIn(note.body)}</strong></p>`;

                await joplin.views.panels.setHtml(panel, wordCountHtml);
            }
        }

        function numberOfWordsIn(body: string) {
            return body.split(' ').length.toString();
        }

        await joplin.workspace.onNoteSelectionChange(() => {
            updateNoteWordCountView();
	});

        await joplin.workspace.onNoteChange(() => {
            updateNoteWordCountView();
        });

        updateNoteWordCountView();
    },
});
